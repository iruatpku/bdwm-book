SHELL=/bin/bash
PDF=mupdf

SRC = preface.tex \
		ToNewbies.tex \
		termbbs.tex \
		firststep.tex \
		onBBS.tex \
		nextstep.tex \
		BM.tex \
		BMOper.tex \
		BMTips.tex \
		BBSClient.tex \
		WWW.tex

all: bdwmbook-view.pdf

bdwmbook.pdf : bdwmbook.tex ${SRC}
	xelatex bdwmbook.tex
	xelatex bdwmbook.tex

bdwmbook-view.pdf : bdwmbook.tex ${SRC}
	sed 's/adobefonts/adobefonts,oneside/g' bdwmbook.tex > bdwmbook-view.tex
	xelatex bdwmbook-view.tex
	xelatex bdwmbook-view.tex
	rm -f bdwmbook-view.tex

view: bdwmbook-view.pdf
	${PDF} bdwmbook-view.pdf

clean:
	rm -f *.{aux,toc,log,out} 

.PHONY: view
